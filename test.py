import numpy as np
import unittest
from sort_million_integers import merge_sort 

class Test(unittest.TestCase):
	def test(self):
		a_list = [54,26,93,17,77,31,44,55,20]
		merge_sort(a_list)


		self.assertTrue(a_list, [17, 20, 26, 31, 44, 54, 55, 77, 93])

	def test_int32(self):
		# a_list = np.random.randint(-65536, 65536, size=1000000, dtype=np.int32)
		# a_list = merge_sort(a_list)
		self.assertTrue(True)

if __name__ == '__main__':
	unittest.main()


