# README #

Bayo Engineer Test
Write a program in any programming language of your preference to sort 1 million of signed 32-bit integers using 3 MB of memory.

### About solution ###

* using merge sort 
* use numpy library to generate random 32 bit integers  array

### How to install ###

* git clone https://gitlab.com/thinhnv.58/bayo-test.git
* virtualenv --python=python3 env 
* source env/bin/activate
* pip install -r requirements.txt
* python test.py
